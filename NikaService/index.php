<?php


$mysqli = new mysqli('localhost', 'root', '', 'crm_simple');
if ($mysqli->connect_error) die("Ошибка подключения: " . $mysqli->connect_error);
$mysqli->set_charset("utf8");

// Добавление нового заказа
if ($_SERVER['REQUEST_METHOD'] === 'POST' && !isset($_GET['edit'])) {
    $clientName = $mysqli->real_escape_string($_POST['client_name']);
    $phone = preg_replace('/[^0-9+]/', '', $_POST['phone']);
    $deviceType = $mysqli->real_escape_string($_POST['device_type']);
    $issue = $mysqli->real_escape_string($_POST['issue']);

    $mysqli->query("
        INSERT INTO orders (client_name, phone, device_type, issue)
        VALUES ('$clientName', '$phone', '$deviceType', '$issue')
    ");
}

// Обновление существующего заказа
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_GET['edit'])) {
    $id = (int)$_GET['edit'];
    $clientName = $mysqli->real_escape_string($_POST['client_name']);
    $phone = preg_replace('/[^0-9+]/', '', $_POST['phone']);
    $deviceType = $mysqli->real_escape_string($_POST['device_type']);
    $issue = $mysqli->real_escape_string($_POST['issue']);

    $mysqli->query("
        UPDATE orders SET
        client_name = '$clientName',
        phone = '$phone',
        device_type = '$deviceType',
        issue = '$issue'
        WHERE id = $id
    ");
    header("Location: index2.php");
}

// Удаление заказа
if (isset($_GET['delete'])) {
    $id = (int)$_GET['delete'];
    $mysqli->query("DELETE FROM orders WHERE id = $id");
    header("Location: index2.php");
}

include 'orders_table.php';
