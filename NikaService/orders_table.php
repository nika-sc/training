<?php $mysqli = new mysqli('localhost', 'root', '', 'crm_simple');
?>
<!DOCTYPE html>
<html>
<head>
    <title>CRM для сервиса</title>
    <style>
        table { width: 100%; border-collapse: collapse; }
        th, td { padding: 8px; border: 1px solid #ddd; text-align: left; }
        .actions { white-space: nowrap; }
    </style>
</head>
<body>
<h1>Сервисный центр</h1>

<!-- Форма добавления -->
<form method="POST">
    <input type="text" name="client_name" placeholder="ФИО клиента" required>
    <input type="tel" name="phone" placeholder="Телефон" required>
    <select name="device_type">
        <option>Ноутбук</option>
        <option>ПК</option>
        <option>Смартфон</option>
    </select>
    <textarea name="issue" placeholder="Описание проблемы" required></textarea>
    <button>Добавить заказ</button>
</form>

<!-- Таблица заказов -->
<h2>Все заказы</h2>
<table>
    <tr>
        <th>ID</th>
        <th>Клиент</th>
        <th>Телефон</th>
        <th>Устройство</th>
        <th>Проблема</th>
        <th>Статус</th>
        <th>Действия</th>
    </tr>
    <?php
    $result = $mysqli->query("SELECT * FROM orders ORDER BY id DESC");
    while($row = $result->fetch_assoc()):
        ?>
        <tr>
            <td><?= $row['id'] ?></td>
            <td><?= htmlspecialchars($row['client_name']) ?></td>
            <td><?= $row['phone'] ?></td>
            <td><?= $row['device_type'] ?></td>
            <td><?= nl2br(htmlspecialchars($row['issue'])) ?></td>
            <td><?= $row['status'] ?></td>
            <td class="actions">
                <a href="edit.php?edit=<?= $row['id'] ?>">✏️</a>
                <a href="index2.php?delete=<?= $row['id'] ?>" onclick="return confirm('Удалить заказ?')">❌</a>
            </td>
        </tr>
    <?php endwhile; ?>
</table>
</body>
</html>