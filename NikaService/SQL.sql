-- Создание базы данных
CREATE DATABASE IF NOT EXISTS crm_simple 
    CHARACTER SET utf8mb4 
    COLLATE utf8mb4_unicode_ci;

USE crm_simple;

-- Таблица заказов
CREATE TABLE IF NOT EXISTS orders (
    id INT PRIMARY KEY AUTO_INCREMENT,
    client_name VARCHAR(100) NOT NULL,
    phone VARCHAR(20) NOT NULL,
    device_type VARCHAR(50),
    issue TEXT NOT NULL,
    status ENUM('Новый', 'В работе', 'Готов') NOT NULL DEFAULT 'Новый',
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Тестовые данные
INSERT INTO orders (client_name, phone, device_type, issue, status) 
VALUES 
    ('Иванов Алексей', '+79001234567', 'Ноутбук', 'Не включается', 'Новый'),
    ('Петрова Мария', '+79007654321', 'Смартфон', 'Разбит экран', 'В работе'),
    ('Сидоров Дмитрий', '+79005554433', 'ПК', 'Не загружается система', 'Готов');