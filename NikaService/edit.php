<?php
$mysqli = new mysqli('localhost', 'root', '', 'crm_simple');
if ($mysqli->connect_error) die("Ошибка подключения: " . $mysqli->connect_error);

// Получение данных заказа
$id = (int)$_GET['edit'];
$order = $mysqli->query("SELECT * FROM orders WHERE id = $id")->fetch_assoc();

if (!$order) {
    die("Заказ не найден!");
}

// Сохранение изменений
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $clientName = $mysqli->real_escape_string($_POST['client_name']);
    $phone = preg_replace('/[^0-9+]/', '', $_POST['phone']);
    $deviceType = $mysqli->real_escape_string($_POST['device_type']);
    $issue = $mysqli->real_escape_string($_POST['issue']);

    $mysqli->query("
        UPDATE orders SET
        client_name = '$clientName',
        phone = '$phone',
        device_type = '$deviceType',
        issue = '$issue'
        WHERE id = $id
    ");
    header("Location: index.php");
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Редактирование заказа</title>
</head>
<body>
<h1>Редактирование заказа #<?= $id ?></h1>
<form method="POST">
    <input type="text" name="client_name" value="<?= htmlspecialchars($order['client_name']) ?>" required><br>
    <input type="tel" name="phone" value="<?= $order['phone'] ?>" required><br>
    <select name="device_type">
        <option <?= $order['device_type'] === 'Ноутбук' ? 'selected' : '' ?>>Ноутбук</option>
        <option <?= $order['device_type'] === 'ПК' ? 'selected' : '' ?>>ПК</option>
        <option <?= $order['device_type'] === 'Смартфон' ? 'selected' : '' ?>>Смартфон</option>
    </select><br>
    <textarea name="issue" required><?= htmlspecialchars($order['issue']) ?></textarea><br>
    <button>Сохранить изменения</button>
    <a href="index.php">Отмена</a>
</form>
</body>
</html>